class Log{
  final String time;
  final String room;
  final String borrow;
  final String borrowed;
  //Enter their name and Student ID
  final String name;
  final String studentId;

  Log({
    this.time,
    this.room,
    this.borrow,
    this.borrowed,
    //Adding to test
    this.name,
    this.studentId,
  });
}