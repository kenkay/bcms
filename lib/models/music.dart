class Music{
  final String instruments;
  final String levels;
  final String hours;
  //Enter their name and Student ID
  final String name;
  final String studentId;

  Music({
    this.instruments,
    this.levels,
    this.hours,
    //Adding to test
    this.name,
    this.studentId,
  });
}