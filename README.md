# BCMS App

A management application for music students and teachers of the BCMS program

## Features:

### Students:
- Register for an instrument and practice time
- Access sheet music and learning materials
- Daily record of practice completed

### Teachers
- Pin verification required
- View registered students
- Check schedule
- Access sheet music and learning materials
- Record attendance 


####[Flutter documentation](https://flutter.dev/docs)
^ Source for: 
tutorials, samples, guidance on dev, and a full API reference.
